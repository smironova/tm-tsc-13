package com.tsc.smironova.tm.util;

public interface SystemOutUtil {

    static void printFailMessage() {
        System.out.println(ColorUtil.RED + "[FAIL]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    static void printOkMessage() {
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

}
