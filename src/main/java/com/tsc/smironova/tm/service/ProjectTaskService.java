package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.IProjectRepository;
import com.tsc.smironova.tm.api.repository.ITaskRepository;
import com.tsc.smironova.tm.api.service.IProjectTaskService;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        if (ValidationUtil.isEmpty(projectId))
            return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (ValidationUtil.isEmpty(projectId) || ValidationUtil.isEmpty(taskId))
            return null;
        return taskRepository.bindTaskToProject(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        if (ValidationUtil.isEmpty(taskId))
            return null;
        return taskRepository.unbindTaskFromProject(taskId);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (ValidationUtil.isEmpty(projectId))
            return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

    @Override
    public Project removeProjectByIndex(final Integer projectIndex) {
        if (ValidationUtil.checkIndex(projectIndex, projectRepository.size()))
            return null;
        final String projectId = projectRepository.findOneByIndex(projectIndex).getId();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneByIndex(projectIndex);
    }

    @Override
    public Project removeProjectByName(final String projectName) {
        if (ValidationUtil.isEmpty(projectName))
            return null;
        final String projectId = projectRepository.findOneByName(projectName).getId();
        if (ValidationUtil.isEmpty(projectId))
            return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneByName(projectName);
    }

    @Override
    public void clearProjects() {
        for (Project project : projectRepository.findAll())
            taskRepository.removeAllByProjectId(project.getId());
        projectRepository.clear();
    }

}
